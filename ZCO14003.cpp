#include <bits/stdc++.h>
#define READ_INT(x) do{register int ch, temp; do{ ch = getchar_unlocked();}while(ch < '0' || ch > '9'); x = 0; while(ch >= '0' && ch <= '9' ){temp = x; x = temp << 3; x += temp << 1; x += (ch - '0'); ch = getchar_unlocked(); } }while(0)
#define For(i, n) for(int i=0;i<((int)n);++i)

using namespace std;

void merge(int arr[], int leftmost, int left_middle, int rightmost)
{
    int right_middle = left_middle + 1;
    int size_of_left_array = left_middle - leftmost + 1;
    int size_of_right_array =  rightmost - right_middle + 1;
    int leftArray[size_of_left_array], rightArray[size_of_right_array];

    for (int counter = 0; counter < size_of_left_array; counter++)
        leftArray[counter] = arr[leftmost + counter];
    for (int counter = 0; counter < size_of_right_array; counter++)
        rightArray[counter] = arr[right_middle + counter];


    int i = 0, j = 0, k = leftmost;
    while (i < size_of_left_array && j < size_of_right_array)
    {
        if (leftArray[i] <= rightArray[j])
        {
            arr[k] = leftArray[i];
            i++;
        }
        else
        {
            arr[k] = rightArray[j];
            j++;
        }
        k++;
    }


    while (i < size_of_left_array)
    {
        arr[k] = leftArray[i];
        i++;
        k++;
    }


    while (j < size_of_right_array)
    {
        arr[k] = rightArray[j];
        j++;
        k++;
    }
}

void mergeSort(int arr[], int leftmost, int rightmost)
{
    if (leftmost < rightmost)
    {
        int left_middle = leftmost+(rightmost-leftmost)/2; //So if an array has 3 elements, the index of middle element will be 0+(3-0)/2 - so index of middle is 1
        int right_middle = left_middle + 1;

        mergeSort(arr, leftmost, left_middle ); // we mergesort sub-array with elements 0 and 1
        mergeSort(arr, right_middle, rightmost); // we mergesort sub-array with element 2

        merge(arr, leftmost, left_middle, rightmost);
    }
}



int main() {
    int number_of_customers;
    unsigned long int max_turnover = 0, turnover = 0;
    READ_INT(number_of_customers);
    int max_price[number_of_customers];
    For(i,number_of_customers)
    {
        READ_INT(max_price[i]);
    }

    mergeSort(max_price, 0, number_of_customers - 1);

    For(i,number_of_customers)
    {
        turnover = (number_of_customers - i)*max_price[i];
        if (turnover > max_turnover)
            max_turnover = turnover;

    }

    printf("%lld\n",max_turnover);





}
